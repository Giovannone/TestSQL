INSERT INTO Anagrafica(Nome,Cognome,CodiceTessera,DataDiNascita) VALUES ('Davide', 'Rossi', '10', '20/04/1995')
INSERT INTO Anagrafica(Nome,Cognome,CodiceTessera,DataDiNascita) VALUES ('Luca', 'Rosa', '11', '10/09/1980')
INSERT INTO Anagrafica(Nome,Cognome,CodiceTessera,DataDiNascita) VALUES ('Paolo', 'Totti', '13', '20/12/1990')
INSERT INTO Anagrafica(Nome,Cognome,CodiceTessera,DataDiNascita) VALUES ('Oreste', 'Ram', '18', '10/02/1087')

INSERT INTO Libri(Titolo,Autore,Genere,Scaffale,CodiceLibro) VALUES ('Il Grande Gatsby','Francis Scott Fitzgerald','Romanzo','Blu',100)
INSERT INTO Libri(Titolo,Autore,Genere,Scaffale,CodiceLibro) VALUES ('Il codice Da Vinci','Dan Brown','Thriller','Rosso',122)
INSERT INTO Libri(Titolo,Autore,Genere,Scaffale,CodiceLibro) VALUES ('Il piccolo principe','Antoine de Saint-Exup�ry','Fantasy','Verde',111)
INSERT INTO Libri(Titolo,Autore,Genere,Scaffale,CodiceLibro) VALUES ('Harry Potter e la pietra filosofale','J.K.Rowling','Fantasy','Verde',131)

INSERT INTO TabPrestiti(Cognome,Titolo,CodiceLibro,CodiceTessera,DataPrestito,DataRestituzione) VALUES ('Rossi','Il codice Da Vinci',122,10,'12/10/2021','22/10/2021')
INSERT INTO TabPrestiti(Cognome,Titolo,CodiceLibro,CodiceTessera,DataPrestito,DataRestituzione) VALUES ('Totti','Harry Potter e la pietra filosofale',131,13,'10/01/2022','22/03/2022')
INSERT INTO TabPrestiti(Cognome,Titolo,CodiceLibro,CodiceTessera,DataPrestito,DataRestituzione) VALUES ('Ram','Il Grande Gatsby',100,18,'19/12/2021','04/01/2022')
INSERT INTO TabPrestiti(Cognome,Titolo,CodiceLibro,CodiceTessera,DataPrestito,DataRestituzione) VALUES ('Rossi','Il Grande Gatsby',100,10,'11/05/2022','19/05/2022')

SELECT* FROM TabPrestiti
